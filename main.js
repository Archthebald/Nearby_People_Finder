// This script controls the main program flow:
// GUI is loaded using Electron.
// MySQL is started.
// Our webserver is started.
// The scan is started.

const {
  app,
  BrowserWindow
} = require('electron')
const path = require('path')
const url = require('url')
const fs = require('fs')
const tcpUtility = require('./scanner/TcpUtility.js')
const dbUtility = require('./db/dbUtility.js')
const webserver = require('./webserver/webserver.js')

/* Global Declarations
 *******************************************************************************/
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win

// All of our functions needed to initialize our program are run here.
// This is the main program flow.
function Initiate() {
  // Start MySQL
  dbUtility.setDB('start');

  // Start webserver with Socket.io
  webserver.Start();

  dbUtility.ConnectDb();

  // Start Python tcpdump scan.
  tcpUtility.StartScan();


}

// Create our GUI window in our Electron ap.
function createWindow() {
  // Create the browser window.
  win = new BrowserWindow({
    width: 800,
    height: 600
  })

  // Use our local webserver to provide GUI.
  win.loadURL('http://localhost:3000/index.html');

  // Open the DevTools.
  // win.webContents.openDevTools()
  // Emitted when the window is closed.
  win.on('closed', () => {
    win = null
  })
}


// When Electron is ready, create window.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q

    app.quit();
});

// When Electron is shutting down, stop our scan and then turn off MySQL.
app.on('quit', () => {

  dbUtility.setDB('stop');
});

app.on('before-quit', () => {
  tcpUtility.KillScan();

});

// If the window is minimized, recreate the GUI.
app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow();
  }
});

app.on('reload', () => {
});

// Start our main application
Initiate();
