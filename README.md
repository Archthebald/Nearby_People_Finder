Team Qubit - Nearby People Finder
=====

Welcome to the GitHub repository for team Qubit's Nearby People Finder! We are a team of students at SELU (Southeastern Louisiana Universtiy) taking Computer Science 401, Survey of Programming Languages. Students in this semester-long course complete programming assignments in a new language each week, which helps students learn concepts while experiencing the capabilities across several languages. In addition, this course involves a semester project where students focus on a language of choice.

Supported devices & installation:
-----
We have tested our app on several Macbook Pro's of varying age (some late 2012, late 2016), and are in the process of testing on Ubuntu. We will offer a convenient package in the future, but for now our app can be installed by running the following commands:
 * Install our app `sudo sh setup.sh`
 * Run our app `sudo npm start`

About the Nearby People Finder:
----
The nearby people finder shows when your friends are nearby, whether they're a floor above you, are across the hall, or are right next to you. We wanted to make an app similar to Find My Friends, but weren't satisfied by inability of GPS to provide accurate proximity indoors. Additionally, not all of our friends used the Find My Friends app. Those that did use the app weren't comfortable sharing their precise location with us. So, we thought "why not find our friends within shouting distance?"

How the Nearby People Finder works:
-----
Our app looks at probe requests sent by nearby devices over Wi-Fi. This works because all mobile devices constantly send probe requests for all Wi-Fi access points they've ever connected to. Suppose my home Wi-Fi is named MAXIENET. Well, my phone, tablet, and laptop are constantly asking "Hey, MAXIENET, are you there?" When your device sends a probe request, it also includes the MAC address of your device, which is essentially a serial number. For my phone, this looks like `2C:59:8A:15:52:14`. The first three numbers `2C:59:8A` correspond to the device manufacturer (in my case, this is LG Electronics). The last three are unique to your device.

Is this legal?  
-----
![alt text](https://media.giphy.com/media/6MzgKYjeIR5EA/giphy.gif)

To the best of our understanding, we believe this is perfectly legal for the following reasons:
 * Consider this good-faith academic security research on the 802.11 Wi-Fi standard. We're making the security suggestion that roaming should be fully encrypted in the 802.11 standard. This past year, the DMCA has specific exceptions for security researchers: https://www.ftc.gov/news-events/blogs/techftc/2016/10/dmca-security-research-exemption-consumer-devices
 * When devices send Wi-Fi probe requests, **there is no specific intended party**. Your phone is simply trying to find internet access by any networks offering it. Suppose you're in Chicago and connect to `attwifi` while enjoying a cheeseburger at a local restaurant. If you happen to travel to New York and enjoy cheeseburgers there, chances are your phone will connect to the same `attwifi` network. This happened because the networks happened to be named the same; the probe request wasn't intended for your specific restaurant in Chicago.
 * **Probe requests are sent in-the-clear with no encryption or obfuscation at all**. Anyone can see your device's probe requests by running `tcpdump` on their computer. This is due to how the IEEE 802.11 Wi-Fi protocol is designed. We aren't breaking any encryption here; our app simply makes it easier for the average person to use and understand the results from `tcpdump`.
 * Some manufacturers randomize device MAC address to prevent tracking. Our app isn't able to track these devices. If you are concerned about your privacy and being tracked, we encourage you to buy a device from a manufacturer that randomizes MAC addresses. Part of our motiviation for this project was from the point-of-view of academic security research. We liken this type of Wi-Fi tracking to a security vulnerability and want to make it obvious to the general public it is possible for individuals to be tracked. The more awareness we raise, the more likely your manufacturer is likely to fix this security vulnerability.

Technologies used:
-----
Node.js: controls execution flow, starting/stopping scans
Python: invokes tcpdump and parses scan results
Electron: GUI using HTML5 & CSS

Our team members:
-----
* Garth Kiepper (team leader)
* Brandon Maulding
* Collin Cashio
* Mitchell Bosman
