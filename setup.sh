# This is our install script. Tested in MacOS.

# Install brew
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

# Install Python
brew install python

# Install Node.js
brew install node

# Install MySQL
brew install mysql

# Install our app
npm install

# Import our DB schema
brew services start mysql && mysql -u root < db/schema.sql && brew services stop mysql

# We might need this for later
brew install portaudio
sudo easy_install pip
sudo pip install pyaudio
