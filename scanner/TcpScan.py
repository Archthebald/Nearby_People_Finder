#!/usr/bin/env python
# This script serves as a wrapper for tcpdump. The Wi-Fi scan is started from this script.
import os
import atexit
import time
import datetime
import subprocess
import sys
import json
import re
import array
from sys import platform

dir_path = os.path.dirname(os.path.realpath(__file__))


# playSound('horn.wav')
# Path must be relative to root of project. When we run sudo node start, the cwd is root of project.
# This imports the manuf parsing library from https://github.com/coolbho3k/manuf
# This library parses the Wireshark manuf file so that we can look up the vendor for a particular MAC.
# global processid
processid = []
pid = os.getpid()
processid.append(pid)

KeyList = []

global OUIList

# Keep track of the total corrupt packets.
bad_packets = 0

# Start scan


def Run_Scan_Daemon(nic):
    global bad_packets

    # Find all network interfaces.
    ifconfig = subprocess.Popen('ifconfig', stdout=subprocess.PIPE)
    tmp = ifconfig.stdout.read()
    interfaces = re.findall(r"(.*\d): ", tmp)
    update_interfaces(interfaces)

    # Start tcpdump scan.
    command = "tcpdump -l -I -e -i {} -s 256 type mgt subtype probe-req -y IEEE802_11_RADIO --immediate-mode".format(
        nic)

    '''Opens Process and reads line buffered output from PIPE: formated for tcpdump output'''
    tcpdump = subprocess.Popen(
        command, bufsize=0, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
    processid.append(tcpdump.pid)

    # Flush initial tcpdump output
    tcpdump.stdout.readline()
    tcpdump.stdout.readline()

    pidsave()

    # Stream IO from tcpdump command and execute this while loop on each new line.
    while True:
        sys.stdout.flush()

        # Fetch output from tcpdump
        scan_result_line = tcpdump.stdout.readline()

        # Check if packet is corrupt and skip if necessary.
        if "bad-fcs" in scan_result_line:
            bad_packets += 1
            continue
        else:
            # Grab source address.
            # MAC: 2c:59:8a:15:52:14
            source_address = re.search(
                r'(?=SA:)...(.{0,17})', scan_result_line)
            try:
                source_address = source_address.group(1).upper()
            except Exception as e:
                source_address = ""

            # Grab signal.
            # Signal: -88
            snr = re.findall(r"(-\d*dBm)", scan_result_line)
            try:
                signal = snr[0][:-3]
            except Exception as e:
                signal = ""

            # Grab noise.
            # Noise: -93
            snr = re.findall(r"(-\d*dBm)", scan_result_line)
            try:
                noise = snr[1][:-3]
            except Exception as e:
                signal = "0"
                noise = "0"

            # Grab antenna.
            # Antenna: 1
            antenna = re.search(r"(antenna (\d*) )", scan_result_line)
            try:
                antenna = antenna.group(2)
            except Exception as e:
                antenna = ""

            # Grab ssid.
            # SSID: silent earth 3
            ssid = re.search(r"Probe Request (?:\((.*)\))", scan_result_line)
            try:
                ssid = ssid.group(1)
            except Exception as e:
                ssid = ""

            # Grab time.
            # Time: 2017-09-17 20:00:07.627949
            timeMicro = re.search(r"(\d*:\d*:\d*\.\d*) ", scan_result_line)
            try:
                timeMicro = (time.strftime("%Y-%m-%d")) + \
                    " " + timeMicro.group(1)
            except Exception as e:
                timeMicro = ""

            # Grab modes.
            # Modes: 6.0 9.0 12.0 18.0 24.0 36.0 48.0 54.0 Mbit
            modes = re.search(r"\[(.*)\]", scan_result_line)
            try:
                modes = modes.group(1)
            except Exception as e:
                modes = ""

            # Grab frequency (in MHz).
            # Frequency: 5745
            frequency = re.search(r"(\d*) MHz", scan_result_line)
            try:
                frequency = frequency.group(1)
            except Exception as e:
                frequency = ""

            if source_address is not "":
                try:
                    if sys.argv[2] is source_address:
                        subprocess.call(["afplay", "{}/horn.wav".format(dir_path)])
                except Exception as e:
                    pass
                # Get a unique tone for each MAC address.
                mac_hash = hash(source_address)

                # Start at the second number of the hash (so the minus is omitted). Grab 2 numbers.
                # We'll pretend this is the nth key on a keyboard (from 0-99).
                key = str(mac_hash)[1:3]

                # Convert this nth key to what it would actually sound like on a keyboard.
                # Formula taken from: https://en.wikipedia.org/wiki/Piano_key_frequencies
                tone = str(2**((float(key) - 49) / 12) * 440)

                # Convert signal dBm to percetange.
                if (int(signal) <= -100):
                    signal = 0
                elif (int(signal) >= -50):
                    quality = 1
                else:
                    quality = 2 * (int(signal) + 100) / 100.00

                vendor = FindOuiVendor(source_address)
                # Prepare JSON output before storing to file.
                output = Device(
                    source_address,
                    signal,
                    noise,
                    antenna,
                    ssid,
                    modes,
                    frequency,
                    timeMicro,
                    vendor
                )

                try:
                    Process_Queue(output)
                    # Save JSON output to file
                    json_save()
                except Exception as e:
                    pass
    return

# Save output scan results as output.json in project root.


def json_save():
    fo = open("{}/output.json".format(dir_path), "wb")
    fo.write(json.dumps(KeyList, default=lambda o: o.__dict__))
    fo.close()


def pidsave():
    with open("{}/.pids.json".format(dir_path), "wb") as outfile:
        json.dump(processid, outfile)


def update_interfaces(arg):
    fo = open("{}/interfaces.json".format(dir_path), "wb")
    fo.write(json.dumps(arg))
    fo.close()

# Constructor for device probe.


class Device(object):
    """docstring for Device."""

    def __init__(self, mac, signal, noise, antenna, ssid, modes, frequency, time, vendor):
        super(Device, self).__init__()
        self.mac_address = mac
        self.signal = signal
        self.noise = noise
        self.antenna = antenna
        self.ssid = ssid
        self.modes = modes
        self.frequency = frequency
        self.time = time
        self.vendor = vendor
        self.name = ""

    def mname(self, iname):
        self.name = iname


def contains(list, filter):
    for x in list:
        if filter(x):
            return True
    return False


def find_dev_obj(name):
    locate = 0
    for value in KeyList:
        if name == KeyList[locate].mac_address:
            return KeyList[locate]
        else:
            locate += 1


def Process_Queue(a):
    # Could make this a little faster and kickout after found, but this is proto
    index = [i for i, x in enumerate(
        KeyList) if x.mac_address == a.mac_address]

    if index == []:
        KeyList.append(a)
    else:
        KeyList[index[0]].sig_strength = a.sig_strength


# Restart Wi-Fi interface when scan is finished so that user can access internet again.
def exit_handler():
    os.system('ifconfig ' + sys.argv[1] +
              ' down && ifconfig ' + sys.argv[1] + ' up')
    sys.exit(0)


def LoadOui():
    global OUIList
    with open("{}/oui.json".format(dir_path)) as json_file:
        OUIList = json.load(json_file)


def FindOuiVendor(oui):
    result = [element['vendor']
              for element in OUIList if element['oui'] == oui[:8]]
    if result:
        return result
    else:
        return ""


# If an error occurs during the scan, kill our thread and output debug info.
LoadOui()

try:
    atexit.register(exit_handler)

    Run_Scan_Daemon(sys.argv[1])
except Exception as e:
    print "Thread Dump"
    print e

sys.exit(0)
