// The main.js script in Node.js invokes this script.
// This script contains the process management for the python scan script
var sudo = require('sudo-prompt');

// var { spawn } = require('child_process');
var interf = require(__dirname+'/interfaces.json')
var settings = require (__dirname+'/Settings.json')

var command = __dirname+'/TcpScan.py '+settings.nic+" "+settings.mac;
var options = {
  name: 'Scan',
  cwd: __dirname,
  encoding: 'utf8',
  timeout: 0,
  maxBuffer: 0
};

// Function to start Wi-Fi scan by invoking TcpScan.py
function StartScan() {
  // console.log("starting scan");

  sudo.exec(command, options,
    function(error, stdout, stderr) {
      if (error) throw error;
    }
  );
}

// Function to stop Wi-Fi scan by killing TcpScan.py and tcpdump.  This is jank
// Tastic
function KillScan() {
  // var pids = require (__dirname+"/.pids.json")
  //
  // for (var i in pids) {
  //   sudo.exec("kill "+pids[i], options, function(error, stdout, stderr){});
  // }
  try {
    // sudo.exec("killall python", options)
    sudo.exec("killall tcpdump", options)
  } catch (e) {

  }
  // process.exit(0);
}

// Expose the start and stop functions to our main program main.js
module.exports.StartScan = StartScan
module.exports.KillScan = KillScan
