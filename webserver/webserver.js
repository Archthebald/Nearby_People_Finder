// The main.js script in Node.js invokes this script.
// This runs a webserver using Express and Socket.io, allowing realtime communication between GUI and backend.

var settings = require (__dirname+'/../scanner/Settings.json')
var express = require('express');
var bodyparser = require('body-parser')
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var path = require('path');
var fs = require('fs');

const tcpUtility = require('../scanner/TcpUtility.js')

// Start webserver
function Start() {
  // Load static files from the gui folder.
  // EXAMPLE:
  // http://localhost:3000/index.html
  // gui/index.html
  app.use(express.static('gui'));
  app.use(bodyparser.urlencoded({
    extended: true
  }));

  // Load our output.json file (still a static file, but not in the gui folder so it needs its own function).
  app.get('/output.json', function(req, res) {
    res.sendFile(path.join(__dirname, '/../scanner/output.json'));
  });

  app.get('/interfaces.json', function(req, res){
    res.sendFile(path.join(__dirname, '/../scanner/interfaces.json'));
  });

  app.get('/TcpUtility.js', function(req, res){
    res.sendFile(path.join(__dirname, '../scanner/TcpUtility.js'));
  });

  // Example of dynamic function in Express.
  app.get('/me', function(req, res){
    res.sendFile(path.join(__dirname, '/../scanner/Settings.json'));
  });

  app.get('/stopscan', function(req,res){
    res.send('STOP');
    tcpUtility.KillScan();
  })
  app.get('/startscan', function(req,res){
    res.send('START');
    tcpUtility.StartScan();
  })

  app.post('/update', function(req,res){
    // console.log(req.body);
    settings = req.body;
    fs.writeFile(__dirname+'/../scanner/Settings.json', JSON.stringify(settings),(err) => {
      if (err) throw err;
    });
    tcpUtility.KillScan();
    tcpUtility.StartScan();
  })



  // Start listening for socket.io communications.
  io.on('connection', function(socket){
    // log when a user connects.
    console.log('a user connected');

    // Example Socket.io function for realtime communication. In the future, SQL will be done this way.
    socket.on('test_function', function(msg) {
      console.log('browser called test_function with: ' + msg);
      // Send response to GUI.
      socket.emit('test_function', 'we got your message');
    });

  });

  http.listen(3000, function(){
    console.log('listening on *:3000');
  });
}

// Expose the start function to our main.js main program so that it can start our webserver.
module.exports.Start = Start
