// This file contains the setDB function, which is responsible for starting and stopping the MySQL database server.
// It is used in main.js when Node.js starts.

const {
  execFile
} = require('child_process');
const fs = require('fs');
var mysql = require('mysql');

// Trims whitespace and newlines. From stackoverflow.
String.prototype.trim = function() {
  return this.replace(/^\s+|\s+$/g, "");
};

var con = null;

// This function starts/stops MySQL.
// I wish there was a better way of doing this than a triple-nested callback. Someone take a crack at it, please.
function setDB(status) {
  // Dynamically fetch location where mysql is installed.
  const child = execFile('which', ["mysql"], (error, stdout, stderr) => {
    if (error) {
      console.log("Error dynamically fetching location where mysql is installed: " + error);
    }

    // Trim the newline after the MySQL installation directory.
    const mysql_location = stdout.trim();

    // Dynamically fetch uid and gid of user that installed mysql.
    fs.stat(mysql_location, (err, stats) => {
      // Error fetching permissions
      if (err) {
        console.log("Error dynamically fetching uid/gid of user that installed mysql: " + err);
      }

      var options = {
        uid: stats['uid'],
        gid: stats['gid']
      }
    });
  });
}


// SQL Functions
function Connect() {
  con = mysql.createConnection({
    host: "localhost",
    user: "scan",
    password: "Secure_password4you",
    database: "scan"
  });
}

function PushScan(ScanData) {
  if (con != null) {

    con.connect(function(err) {

      if (err) throw err;
      sql = "INSERT INTO devices (mac, name, vendor, vendor_comment) VALUES ();"

      con.query(sql, function(err, result) {
        if (err) throw err;
        console.log("Result: " + result);
      });

      // console.log("Connected!");

    });

  }
}

function PullScan() {
  if (con != null) {
    con.connect(function(err) {
      if (err) throw err;

      con.query(sql, function(err, result) {
        if (err) throw err;
        console.log("Result: " + result);
      });
      console.log("Connected!");
    });
  }
}

function Disconnect() {
  con = null;
}

// Export the setDB function so main.js can call it.
module.exports.setDB = setDB
module.exports.ConnectDb = Connect
module.exports.PushScan = PushScan
module.exports.PullScan = PullScan
module.exports.Disconnect = Disconnect
