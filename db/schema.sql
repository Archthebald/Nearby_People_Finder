CREATE DATABASE IF NOT EXISTS `scan`;

USE `scan`;

# This data never changes for a device.
CREATE TABLE IF NOT EXISTS `devices` (
  `mac` char(12) NOT NULL,
  `name` varchar(255),
  `vendor` varchar(255),
  `vendor_comment` varchar(255)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `devices`
  ADD PRIMARY KEY `mac` (`mac`) USING BTREE;

# This data is unique to each probe sent by a device.
CREATE TABLE probes (
  `mac` char(12) NOT NULL,
  `signal` tinyint NOT NULL,
  `noise` tinyint NOT NULL,
  `time` datetime(6) NOT NULL,
  `antenna` tinyint unsigned NOT NULL,
  `modes` varchar(255) NOT NULL,
  `ssid` varchar(255) NOT NULL,
  `frequency` tinyint unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `probes`
  ADD PRIMARY KEY `mac` (`mac`) USING BTREE;

ALTER TABLE `probes`
  ADD INDEX `time` (`time`) USING BTREE;
