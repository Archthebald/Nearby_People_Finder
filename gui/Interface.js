// This file contains all display functions that require jQuery.
// todo: refactor

function ref() {
  location.reload();
}

function startSorts() {
  $(function() {
    $("#myTable").tablesorter();
  });
}

function KillScan() {
  $.get("http://localhost:3000/stopscan");
}

function StartScan() {
  $.get("http://localhost:3000/startscan");
}

function update() {
  var settings = {
    "nic": $(document.getElementById('nope')).val(),
    "mac": $(document.getElementById('alarm')).val()
  }
  $.post("http://localhost:3000/update", settings)
  console.log(settings);

}
// Sort values in table.
function sortTable(f, n) {
  var rows = $('#myTable tbody  tr').get();

  rows.sort(function(a, b) {

    var A = getVal(a);
    var B = getVal(b);

    if (A < B) {
      return -1 * f;
    }
    if (A > B) {
      return 1 * f;
    }
    return 0;
  });

  function getVal(elm) {
    var v = $(elm).children('td').eq(n).text().toUpperCase();
    if ($.isNumeric(v)) {
      v = parseInt(v, 10);
    }
    return v;
  }

  $.each(rows, function(index, row) {
    $('#myTable').children('tbody').append(row);
  });
}
var f_tname = 1;
var f_tsig = 1;
var f_tmanuf = 1;
var f_tmac = 1;

$("#tname").click(function() {
  f_tname *= -1;
  var n = $(this).prevAll().length;
  sortTable(f_tname, n);
});
$("#tsig").click(function() {
  f_tsig *= -1;
  var n = $(this).prevAll().length;
  sortTable(f_tsig, n);
});
$("#tmanuf").click(function() {
  console.log("tmanuf active");
  f_tmanuf *= -1;
  var n = $(this).prevAll().length;
  sortTable(f_tmanuf, n);
});
$("#tmac").click(function() {
  f_tmac *= -1;
  var n = $(this).prevAll().length;
  sortTable(f_tmac, n);
});

var obj;

// Get current scan report.
jQuery.get('output.json', function(data) {
  obj = data;
}).done(function() {
  // Display and sort results.
  display();
  startSorts();
});
// Fresnel distance calculation converting signal dBm and frequency in MHz to feet.
function calculateDistance(signal, frequency) {
  return Math.floor(Math.pow(10.0, (27.55 - (20 * Math.log10(frequency)) + Math.abs(signal)) / 20.0) * 3.28084);
}

// Display table.
function display() {
  var myTable = "<table class='table table-hover tablesorter' name='myTable' id='myTable'><thead><tr>";
  //myTable += "<th><span style='cursor:pointer'>Name</span></th>";
  myTable += "<th id='tmanuf'><span style='cursor:pointer'>Manufacturer</span></th>";
  myTable += "<th id='tmanuf'><span style='cursor:pointer'>SSID</span></th>";
  myTable += "<th id='tsig'><span style='cursor:pointer'>Distance (ft)</span></th>";
  myTable += "<th id='tsig'><span style='cursor:pointer'>Signal (dBm)</span></th>";
  // myTable += "<th id='tsig'><span style='cursor:pointer'>Noise (dBm)</span></th>";
  myTable += "<th id='tmac'><span style='cursor:pointer'>MAC Address</span></th>";
  // myTable += "<th id='tmanuf'><span style='cursor:pointer'>Antenna</span></th>";
  myTable += "<th id='tmanuf'><span style='cursor:pointer'>Frequency (MHz)</span></th>";
  myTable += "<th id='tmanuf'><span style='cursor:pointer'>Modes</span></th>";
  myTable += "</tr></thead><tbody>";

  // Display devices rows in table.
  for (var i = 0; i < obj.length; i++) {
    myTable += "<tr>";
    //myTable += "<td>" + obj[i].name + "</td>";
    myTable += "<td>" + obj[i].vendor + "</td>";
    myTable += "<td>" + obj[i].ssid + "</td>";
    var distance = calculateDistance(obj[i].signal, obj[i].frequency);
    myTable += "<td>" + distance + "</td>";
    myTable += "<td>" + obj[i].signal + "</td>";
    // myTable += "<td>" + obj[i].noise + "</td>";
    myTable += "<td>" + obj[i].mac_address + "</td>";
    // myTable += "<td>" + obj[i].antenna + "</td>";
    myTable += "<td>" + obj[i].frequency + "</td>";
    myTable += "<td>" + obj[i].modes + "</td>";
    myTable += "</tr>";
  }

  myTable += "</tbody></table>";
  document.getElementById('tableeee').innerHTML = myTable;
}

function populate() {
  var obj2 = [];
  var options = "<select id ='nope' class='form-controls'>";

  $.get("me", function(ret) {
    settings = ret;
  }).done(function() {
    // document.getElementById('alarm').val(s)
    $("#alarm").val(settings.mac);
      $.get("interfaces.json", function(data) {
        obj2 = data;
      }).done(function() {

        for (var j = 0; j < obj2.length; j++) {
          if (obj2[j] == settings.nic) {
            options += "<option selected>" + obj2[j] + "</option>";
          }else {
            options += "<option>" + obj2[j] + "</option>";
          }

        }
        options += "</select>";
        document.getElementById('testttt').innerHTML = options;
      });
    })
  }
