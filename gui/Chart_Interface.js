// This file contains all display functions that require jQuery.
var obj;
var macArray = [];
var colorArray = [];
var distanceArray = [];

// I need to get a file that we save from the output.json 
jQuery.get('output.json', function(data) {
  obj = data;
}).done(function() {
    setMacArray();
    setColorArray();
    setDistanceArray();
}).done(function(){
    loadMap(distanceArray);
});
    
// Fresnel distance calculation converting signal dBm and frequency in MHz to feet.
function calculateDistance(signal, frequency) {
    return Math.floor(Math.pow(10.0, (27.55 - (20 * Math.log10(frequency)) + Math.abs(signal)) / 20.0) * 3.28084);
}

function setDistanceArray(){
    for (var i = 0; i < macArray.length; i++) {
        var distance = calculateDistance(obj[i].signal, obj[i].frequency);
        distanceArray.push(distance);
    }
}

function randomColor()
{
     return 'rgba('+Math.round(Math.random()*255)+','+Math.round(Math.random()*255)+','+Math.round(Math.random()*255)+',0.4)';
}

function setColorArray(){
    for (var i = 0; i < macArray.length; i++) {
        colorArray.push(randomColor());
    }
}

function setMacArray(){
    for (var i = 0; i < obj.length && i <= 20; i++) {
        macArray.push(obj[i].mac_address);
    }
}


//  all the code needed for displaying in pie chart
function loadMap(distanceArray){
  var chartColors = window.chartColors;
  var color = Chart.helpers.color;
  var config = {
      data: {
          datasets: [{
              data: distanceArray,
              backgroundColor: colorArray,
          }],       
          labels: macArray
      },
      
      options: {
        startAngle: Math.PI / 2,
        responsive: true,
        legend: {
            position: 'left'
        },
        title: {
            display: true
        },
        scale: {
            ticks: {
              beginAtZero: true
            },
            reverse: false
        },
        animation: {
            animateRotate: false,
            animateScale: true
          }
      }
  };
    var ctx = document.getElementById("chart-area");
    window.myPolarArea = Chart.PolarArea(ctx, config);
}